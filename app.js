// Express Setup

const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes')
const courseRoutes = require('./routes/courseRoutes')

const app = express();

// process.env.PORT means that heroku can decide..
const port = process.env.PORT || 4000;


// Middlewares:
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Main URI:
app.use("/users", userRoutes)
app.use("/courses", courseRoutes)


// Mongoose Connection
mongoose.connect(`mongodb+srv://reddelacruzwebsite:admin123@cluster0.itpeetc.mongodb.net/s37-s41?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

const db = mongoose.connection

db.on('error', () => console.error('Connection Error'))
db.once('open', () => console.log('Connected to MongoDB!'))


app.listen(port, () => console.log(`API is now online at port: ${port}`));