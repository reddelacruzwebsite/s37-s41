const User = require('../models/User');
const Course = require('../models/Course');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// checkEmailExists function
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
};

// registerUser function
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
};

// loginUser function
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		} else {
			// compareSync(<dataToCompare>, <encrypted password>)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}


		}
	})
 
}


// getProfile function
// module.exports.getProfile = (reqBody) => {
// 	return User.findOne({id: reqBody.id}).then(result => {
// 		if(result == null) {
// 			return false
// 		} else {

// 			result.password = "*****"
		
// 			return result;

// 		}
// 	})
 
// }


// module.exports.getProfile = (data) => {

// 	return User.findById(data.userId).then(result => {

// 		console.log(result)

// 		result.password = "";

// 		// Returns the user information with the password as an empty string
// 		return result;

// 	});

// };



module.exports.getProfile = (userData) => {

	return User.findById(userData.id).then(result => {

		console.log(userData)

		if(result == null) {
			return false
		} else {
		console.log(result)

		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

		}

	});

};


module.exports.enroll = async (data) => {

	if(data.isAdmin == false ) {
	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId})

		return user.save().then((user, error) => {

			if(error) {
				return false
			} else {
				return true
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({userId: data.userId})

		return course.save().then((course, error) => {
			if(error) {
				return false 
			} else {
				return true
			}
		})
	})

	if (isUserUpdated && isCourseUpdated) {
		return true //"User successfully enrolled"
	} else {
		return false //"Client cannot enroll. Error encountered."
	}
} else {
	return "Admins cannot enroll to a course."
}
};













